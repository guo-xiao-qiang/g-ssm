package com.hxci.G.pojo;

public class studentG {
    private Integer idG;
    private String nameG;
    private Integer ageG;
    private String snoG;

    public Integer getIdG() {
        return idG;
    }

    public void setIdG(Integer idG) {
        this.idG = idG;
    }

    public String getNameG() {
        return nameG;
    }

    public void setNameG(String nameG) {
        this.nameG = nameG;
    }

    public Integer getAgeG() {
        return ageG;
    }

    public void setAgeG(Integer ageG) {
        this.ageG = ageG;
    }

    public String getSnoG() {
        return snoG;
    }

    public void setSnoG(String snoG) {
        this.snoG = snoG;
    }
}
