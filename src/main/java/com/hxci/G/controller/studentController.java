package com.hxci.G.controller;

import com.hxci.G.dao.studentDao;
import com.hxci.G.pojo.studentG;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("G")
public class studentController {
    @Autowired
    studentDao studentdao;
    @RequestMapping("query")
    public String query(Model model){
        List<studentG> list = studentdao.query();
        model.addAttribute("list",list);
        return "list";
    }

    @RequestMapping("delete")
    public String del(Integer idG){
        studentdao.delete(idG);
        return "redirect:query";
    }

    @RequestMapping("jumpPage")
    public String  jumpPage(){
        return "add";
    }
    
    @RequestMapping("add")
    public String add(studentG student){
        studentdao.insert(student);
        return "redirect:query";
    }


//    @RequestMapping("upquery")
//    public String upquery(Integer idG,Model model){
////        student student = studentdao.upquery(idG);
//        model.addAttribute("student",student);
//        return "update";
//    }


    @RequestMapping("update")
    public String update(studentG student){
        studentdao.update(student);
        return "redirect:query";
    }

}
