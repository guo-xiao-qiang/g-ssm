package com.hxci.G.dao;

import com.hxci.G.pojo.studentG;

import java.util.List;

public interface studentDao {
//    查询student表所有数据
    public List<studentG> query();

//    删除数据
    public void delete(Integer id);

//    插入数据
    public void insert(studentG student);

//    修改数据
    public void update(studentG student);
}
