﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() +"://"+ request.getServerName()
            +":"+request.getServerPort()+path;
%>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title></title>
<link rel="stylesheet" href="<%=basePath%>/css/pintuer.css">
<link rel="stylesheet" href="<%=basePath%>/css/admin.css">
<script src="<%=basePath%>/js/jquery-2.1.0.js"></script>
<script src="<%=basePath%>/js/pintuer.js"></script>
</head>
<body>
<form  id="listform">
  <div class="panel admin-panel">
    <div class="panel-head"><strong class="icon-reorder"> 学生列表</strong> <a href="" style="float:right; display:none;">添加学生</a></div>
    <div class="padding border-bottom">
      <ul class="search" style="padding-left:10px;">
        <li> <a class="button border-main icon-plus-square-o" href="<%=basePath%>/tea/add"> 添加学生</a> </li>
        
        <li>
          <input id="iname"  type="text" placeholder="请输入教师名称" name="keywords" class="input" style="width:250px; line-height:17px;display:inline-block" />
          <a href="javascript:void(0)" class="button border-main icon-search" onclick="namesearch()" > 搜索</a></li>
      </ul>
    </div>
    <table id="teachertable" class="table table-hover text-center">
		<tr><th width="100" style="text-align:left; padding-left:20px;">学号</th> <th>学生名称</th>  <th>学生年龄</th><th width="310">操作</th> </tr>
    <c:forEach items="${list}" var="a">

        <tr><td width="100" style="text-align:left; padding-left:20px;">${a.snoG}</td><td>${a.nameG}</td>  <td>${a.ageG}</td><td width="310"><a href="<%=basePath%>/G/delete?idG=${a.idG}">删除</a><a
                href="<%=basePath%>/G/jumpPage">添加</a><a href="<%=basePath%>/G/update?idG=${a.idG}">删除</a></td> </tr>
    </c:forEach>
    </table>
     <tr>
    	<td colspan="8">
    		<div class="pagelist"> 
    			<a id='first_page'  href="#">首页</a> 
    			<a id='last_page'   href="#">上一页</a> 
    		    <a id='next_page'   href="#">下一页</a>
    		    <a id='end_page'    href="#">尾页</a>
    		    <a id='jump_page'   href="#">跳转</a>
    		    <input id="page_no" style="height: 20px;width:30px"/>
    		    <input id="total_page"style="height: 20px;width:50px"  value="1 页"/>
    		</div>
    	</td>
    </tr>
    
  </div>
</form>
</body>
</html>