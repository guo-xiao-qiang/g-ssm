﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() +"://"+ request.getServerName()
            +":"+request.getServerPort()+path;
%>

<!DOCTYPE html>
<html lang="zh-cn">
	
<style type="text/css">
    	#preview{
width:100px;
height:100px;
margin:20px auto;
cursor: pointer;
}
    </style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="renderer" content="webkit">
<title></title>
<link rel="stylesheet" href="<%=basePath%>/css/pintuer.css">
<link rel="stylesheet" href="<%=basePath%>/css/admin.css">
<script src="<%=basePath%>/js/jquery-2.1.0.js"></script>
<script src="<%=basePath%>/js/pintuer.js"></script>
<script src="<%=basePath%>/js/ajaxfileupload.js" type="text/javascript" charset="utf-8"></script>

</head>
<body>
<div class="panel admin-panel">
  <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>增加教师</strong></div>
  <div class="body-content">
    <form action="<%=basePath%>/G/add" method="post" class="form-x" >


        <div class="form-group">
            <div class="label">
                <label>学号：</label>
            </div>
            <div class="field">
                <input   type="text" class="input w50"  name="snoG" data-validate="required:请输入学号" />
                <div class="tips"></div>
            </div>
        </div>

      <div class="form-group">
        <div class="label">
          <label>学生名称：</label>
        </div>
        <div class="field">
          <input   type="text" class="input w50"  name="nameG" data-validate="required:请输入学生名称" />
          <div class="tips"></div>
        </div>
      </div>
      
   
          
      
        <div class="form-group">
          <div class="label">
            <label>学生年龄：</label>
          </div>
            <div class="field">
                <input   type="text" class="input w50"  name="ageG" data-validate="required:请输入学生年龄" />
                <div class="tips"></div>
            </div>
        </div>
        </div>

     
      <div class="form-group">
        <div class="label">
          <label></label>
        </div>
        <div class="field">
         <input  class="button bg-main icon-check-square-o"  style="text-align: center;"  type="submit" name="back" value="确定提交" />
        </div>
      </div>
    </form>
  </div>
</div>

</body>




</html>